var class_real =
[
    [ "Real", "class_real.html#aa699b561dbfa8406e76f0063366dfc76", null ],
    [ "~Real", "class_real.html#a7d77a4cab99668bd6e6a4f66ce397b09", null ],
    [ "calcFlags", "class_real.html#a43e4fd941ed4454b005891351a352305", null ],
    [ "getForces", "class_real.html#a623a4a20254fe477e9b0f4e189b1d125", null ],
    [ "getForceX", "class_real.html#ad4b50928cc77e1f00eb2044f456680a0", null ],
    [ "getForceY", "class_real.html#ac9fe4d251f7d3829a58280c2e75948ae", null ],
    [ "getForceZ", "class_real.html#a38496c99452394d4885818204b4d1baf", null ],
    [ "getNumber", "class_real.html#a62c3c4af0f214e8fd8ec8baf01f2b872", null ],
    [ "getVelocities", "class_real.html#a51e4659d3fb7f2e80ef5cce03b5783f7", null ],
    [ "getVelocityX", "class_real.html#ade8eb7a375fc86cf83416a78a839c75d", null ],
    [ "getVelocityY", "class_real.html#afb152dc2baa3aa357fa51b7871395729", null ],
    [ "getVelocityZ", "class_real.html#a5e7e01adf2ba25cb523bc612162241c7", null ],
    [ "move", "class_real.html#a9bc3524d0db29e83bcb86496b61bab14", null ],
    [ "setForces", "class_real.html#add6a81135f3db4574f685578f468a885", null ]
];