var searchData=
[
  ['setcoords_119',['setCoords',['../class_particle.html#a82ab9478bebff4d1748e7a1d0b7e6df7',1,'Particle']]],
  ['setdensity_120',['setDensity',['../class_particle.html#ae819aa61bd33a2773615c29e24511edb',1,'Particle']]],
  ['setforces_121',['setForces',['../class_real.html#add6a81135f3db4574f685578f468a885',1,'Real']]],
  ['setmass_122',['setMass',['../class_particle.html#a98ecbe7072d66cf9e8ba0f41cd40574e',1,'Particle']]],
  ['setplot_123',['setPlot',['../class_functions.html#a25583dabe1b9551332fe3d84bc442a86',1,'Functions']]],
  ['setradius_124',['setRadius',['../class_particle.html#ac005e3a0a82d8e4d75a704c47bf31adb',1,'Particle']]]
];
