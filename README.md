**Disclaimer:** Attempt to write particle dynamic system using object C++ and Python. It will use classical force fields.

[Project documentation](https://uklaad.gitlab.io/particle-dynamic)

# **VS Code setup**

1. `sudo apt install g++ gcc libtbb-dev python3 cmake gnuplot`
2. Install VS Code extensions - `C/C++, C/C++ Extension Pack, C/C++ Themes, Better Syntax, Office Viewer, CMake, CMake Tools`.
3. Check  `File -> Auto Save`
4. **Git:** Directly from GitLab `"Clone" -> Visual Studio Code (SSH)` and set the working directory. Then set up (if you are the author):
   `git config --global user.name "uklaad"`
   `git config --global user.email halda@riseup.net`

* **Just optional info:**
  * Init CMake for new projects by `Ctrl+Shift+P -> CMake: Quick Start`
  * To use c++20 in code review: `Ctrl+, -> cppstandard -> change the default settings`

##### **Doxygen documentation in VS Code:**

1. `sudo apt install doxygen-gui`
2. VS Code extensions `Doxygen Runner` and `Doxygen Documentation Generator`
3. Create `./doxygen` folder and copy the `Doxyfile` from this project there, which was already [edited](https://www.youtube.com/watch?v=TtRn3HsOm1s) in the GUI. The `./doxygen` will become the root Doxygen folder
4. In `Doxyfile` look at and change accordingly (can be also modified with GUI = run `doxywizard` in terminal):

   * `PROJECT_NAME`
   * `PROJECT_NUMBER`
   * `PROJECT_BRIEF`
   * `PROJECT_LOGO` (required picture of ~70px height)
   * `OUTPUT_DIRECTORY` (recommended to leave it)
   * `INPUT`
5. With opened `Doxyfile` in VS Code, hit `F1` and run `Generate Doxygen documentation`
6. To start documenting you code, write `/**` right above your declaration and hit enter.

   * [Valid Doxygen tags](https://new.rosettacommons.org/docs/latest/development_documentation/tutorials/doxygen-tips)
7. Then you can [deploy the documentation on GitLab](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ci_cd_template.html).

**Some good additional info:**

* [VSC begin](https://www.youtube.com/watch?v=FcYs8wtzjVE) ; [C++ full course](https://www.youtube.com/watch?v=6y0bp-mnYU0) ; [Obj C++](https://www.youtube.com/watch?v=wN0x9eZLix4) ; [Md Flowcharts](https://mermaid-js.github.io/mermaid/#/flowchart)
* Making gif: `ffmpeg -framerate 24 -pattern_type glob -i '*.png' out.gif`

**MatPlot++**

* The git downloader is already inserted in project's `CMakeLists.txt` just copy it to an another project if needed.
* [install](https://github.com/alandefreitas/matplotplusplus) ; [example](https://alandefreitas.github.io/matplotplusplus/plot-types/data-distribution/scatter-plot-3d/)

# **How the code works**
Square 5 is the Real unit box with Real particles (blue) and calculations are done on extended space with Virt particles' duplicates (green) which simulates the infinite space environment. :
![img](./doxygen/RealVirtSpace.png)
```mermaid
flowchart TD
    S((Start at\n t = 0.0 s)) --> Real[("Real particle creation")]
    Real---RealSub{{"Is it the right time for creation? \n Set random coords within XY plane.\n Set random velocity #40;optional#41;.\n Set random radius from PDF.\n New can not overlap with existing Real. \n Exit strategy for impossible creation."}}
    Real--->Virt[("Virt particles creation \n #40;duplicate from Real#41;")]
  
    Virt-->ForcesReal["Real#58; summarize forces"]
    ForcesReal---ForcesRealSub{{"Environmental forces acting on Real. \n Forces between Real particles. \n Forces between Real and Virt particles."}}
    ForcesReal--->NewCoordsReal["Real#58; calc new coords"]
  NewCoordsReal---NewCoordsRealSub{{"Use Newtonian motion equations. \n Is the particle outside the box? \n Didn't it move too far? \n #40;Acceleration, Velocity, Time step #916;t #41;"}}
  NewCoordsReal--->Plot["Real#58; plot particles"]
  Plot-->Decision{Max time? \n Max particles' number? \n Error exception?}
  Decision--"ADD #916;t"-->Real
  Decision-->E((End. \n Output is \n video file))
```
```mermaid
classDiagram
      Particle <|-- Real
      Particle <|-- Virt
      class Particle{
         array<double, 3> 	Coords
         double 	Radius
         double 	Density
         double 	Mass
      }
      class Real{
         int Number
         array<double, 3> Velocities
         array<double, 3> Forces
      }
      class Virt{
      }
```
Particles are randomly generated in a way that they do not overlap. Here, 72 particles were generated in XY plane with maximum of 100,000 generation attempts.
![img](./doxygen/ParticleGenerationReal.gif)



**Example**

Free movement with initial random velocities up to 10 nm/s, no interactions. The unit cell is [ 0 ; 1000 ] nm.
2D and 3D view of the same situation:
![img](./doxygen/2D_velocity.gif)
![img](./doxygen/3D_velocity.gif)
