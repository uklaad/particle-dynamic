#include <iostream>
#include <fstream>

#define CONFIG_FILE_PATH "./parameters.conf"

double CoordLimit; // [nm]
double VelocityLimit; // [nm/s]
double Density; //[kg/m3]
double MaxPartNumber; // [-]
double Mu; // [nm]
double Sigma; // [nm]
double deltaTime; // [s]

bool LoadParameters()
{
  std::ifstream in(CONFIG_FILE_PATH);

  if (!in.is_open())
  {
    std::cout << "Cannot open configuration file from: " << CONFIG_FILE_PATH << std::endl;
    return false;
  }

  std::string param;
  double value;

  while (!in.eof())
  {
    in >> param;
    in >> value;

    if (param == "CoordLimit:") { CoordLimit = value; }
    else if (param == "VelocityLimit:") { VelocityLimit = value; }
    else if (param == "Density:") { Density = value; }
    else if (param == "MaxPartNumber:") { MaxPartNumber = value; }
    else if (param == "Mu:") { Mu = value; }
    else if (param == "Sigma:") { Sigma = value; }
    else if (param == "deltaTime:") { deltaTime = value; }
  }

  in.close();

  return true;
}