/*
This file defines particles
and temporary particles that are used to infinite periodic space calculation
Constructors should be public!
Use "static" if it does not operate on object
*/
#pragma once // Ensure the h file is used only once in cpp
#include <iostream>
#include <vector>
#include <algorithm>
#include <execution>
#include <random>
#include <matplot/matplot.h>
#include <tuple>
#include <string>
#include <cmath>

extern std::default_random_engine generator; // Global random generator defined in main.cpp header
extern double nm2m;
/*###################
# GENERAL FUNCTIONS #
###################*/
class Functions
{
public:
    static void setPlot(double& CoordLimit);
    static void plotParticle(double x, double y, double z, double r);
    static void figSave(int& fig_number);
    static void pushToForceVector(double& F, std::array<double, 3>& unit_R,
                                  std::vector<double>& vec_forceX_A, std::vector<double>& vec_forceY_A, std::vector<double>& vec_forceZ_A);
    static bool testCollision(std::array<double, 3> coordsA, double radiusA, std::array<double, 3> coordsB, double radiusB); // Serve particle and go through real_particle vector
};

/*################
# OBJECT CLASSES #
################*/
class Particle
{
protected:
    std::array<double, 3> Coords; // [nm]
    double Radius;                // [nm]
    double Density;               // [kg/m3]
    double Mass;                  // [kg]
    void setCoords(std::array<double, 3> coords) { Coords = coords; }
    void setRadius(double radius) { Radius = radius; }
    void setDensity(double density) { Density = density; }
    void setMass(double mass) { Mass = mass; }

public:
    std::array<double, 3> getCoords() { return Coords; }
    double getCoordX() { return Coords.at(0); }
    double getCoordY() { return Coords.at(1); }
    double getCoordZ() { return Coords.at(2); }
    double getRadius() { return Radius; }
    double getDensity() { return Density; }
    double getMass() { return Mass; }

    static std::tuple<double, double, double> calcForce(std::array<double, 3> coordS_A, double Radius_A,
                                                        std::array<double, 3> coordS_B, double Radius_B);
};

class Real : public Particle
{ // Particles inside a box
private:
    short int Number;

    std::array<double, 3> Velocities; // [nm/s]
    std::array<double, 3> Forces;     // [N]
    short int Accel_flag;
    short int Velocity_flag;
    short int Overlap_flag;

    // Setters for Real
    void setNumber(int& number) { Number = number; }
    
    void setVelocities(std::array<double, 3> velocities) { Velocities = velocities; }

    void setLimit_accel_flag(short int Af) { Accel_flag = Af; }
    void setHigh_velocity_flag(short int Vf) { Velocity_flag = Vf; }
    void setOverlap_flag(short int Of) { Overlap_flag = Of; }

    // Private Functions for Real
    double genRandCoord(double CoordLimit)
    {
        std::uniform_real_distribution<double> distribution(0, CoordLimit); //(from, to)
        return distribution(generator);
    }
    double genRandVelocity(double VelocityLimit)
    {
        std::uniform_real_distribution<double> distribution(-VelocityLimit, VelocityLimit); //(from, to)
        return distribution(generator);
    }
    double genRandRadius(double Mu, double Sigma)
    {                                                                // http://www.cplusplus.com/reference/random/lognormal_distribution/
        std::lognormal_distribution<double> distribution(Mu, Sigma); //(mu, sigma) https://keisan.casio.com/exec/system/1180573214
        return distribution(generator);
    }

public:
    Real(int& number, double& CoordLimit, double& VelocityLimit, double& Mu, double& Sigma, double& Density)
    { // constructor
        setNumber(number);
        setCoords({genRandCoord(CoordLimit), 0, 0});
        setVelocities({genRandVelocity(VelocityLimit), genRandVelocity(VelocityLimit), 0});
        setForces({0, 0, 0});
        setRadius(genRandRadius(Mu, Sigma));
        setDensity(Density);
        setMass(getDensity() * 4 / 3 * matplot::pi * pow(getRadius() * nm2m, 3)); // OK checked
    }
    ~Real()
    {
    }
    static void printRealProperties(std::vector<Real>& real_particles);
    

    void calcFlags()
    {
        // 2. TODO check if something is wrong with particle force / velocity prior to calcNewCoords
    }
    void move(double deltaTime, double CoordLimit);
    static void plotParticles(std::vector<Real>& real_particles);
    
    void setForces(std::array<double, 3> forces) { Forces = forces; }
    // Getters for Real
    std::array<double, 3> getVelocities() { return Velocities; }
    std::array<double, 3> getForces() { return Forces; }
    double getVelocityX() { return Velocities.at(0); }
    double getVelocityY() { return Velocities.at(1); }
    double getVelocityZ() { return Velocities.at(2); }
    double getForceX() { return Forces.at(0); }
    double getForceY() { return Forces.at(1); }
    double getForceZ() { return Forces.at(2); }
    short int getNumber() { return Number; }
};

class Virt : public Particle
{

public:
    static void plotParticles(std::vector<Virt> virt_particles);

    Virt(double x, double y, double z, double r)
    {
        setCoords({x, y, z}); // generate 12369874 space from Real
        setRadius(r);
    }
    ~Virt()
    {
    }
};