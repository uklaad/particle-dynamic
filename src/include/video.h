#ifdef _MSC_VER
#include <Windows.h> //For Sleep(1000)
#endif
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"
#include <stdio.h>

void makeVideo(std::list<std::vector<uchar>> jpeg_frames, int FPS)
{
    std::ostringstream oss;
    oss << "ffmpeg -y -f image2pipe -r " << FPS << " -i pipe: -codec copy video.avi"; // You may want to use full path: "/usr/bin/ffmpeg -y ..."
    std::list<std::vector<uchar>>::iterator it;

// In Windows (using Visual Studio) we need to use _popen and in Linux popen
#ifdef _MSC_VER
    // ffmpeg.exe must be in the system path (or in the working directory)
    FILE *pipeout = _popen(oss.str().c_str(), "wb"); // For Windows use "wb"
#else
    // https://batchloaf.wordpress.com/2017/02/12/a-simple-way-to-read-and-write-audio-and-video-files-in-c-using-ffmpeg-part-2-video/
    FILE *pipeout = popen(oss.str().c_str(), "w"); // For Linux use "w"
#endif

    // Iterate list of encoded frames and write the encoded frames to pipeout
    for (it = jpeg_frames.begin(); it != jpeg_frames.end(); ++it)
    {
        std::vector<uchar> jpeg_img = *it;

        // Write this frame to the output pipe
        fwrite(jpeg_img.data(), 1, jpeg_img.size(), pipeout);
    }
    // Flush and close input and output pipes
    fflush(pipeout);

#ifdef _MSC_VER
    _pclose(pipeout);
    Sleep(1000); // It looks like we need to wait one more second at the end.
#else
    pclose(pipeout);
#endif
}