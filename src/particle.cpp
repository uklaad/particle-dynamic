#include "include/particle.h"

/*###################
# GENERAL FUNCTIONS #
###################*/
void Functions::setPlot(double &CoordLimit)
{
    auto fig = matplot::figure(true); // false for fig draw - show window at every change (not good)
    fig->size(1000, 1000);
    auto graph = matplot::gca();
    graph->box_full(true);

    /* matplot::xlim({0, CoordLimit});
    matplot::ylim({0, CoordLimit});
    matplot::zlim({0, CoordLimit}); */

    matplot::xlim({-CoordLimit, CoordLimit * 2});
    matplot::ylim({-CoordLimit, CoordLimit * 2});
    matplot::zlim({-CoordLimit, CoordLimit * 2});

    matplot::xlabel("X [nm]");
    matplot::ylabel("Y [nm]");
    matplot::zlabel("Z [nm]");
    matplot::title("Test");
    matplot::box(matplot::on);
    matplot::view(2); // XY view
}
void Functions::figSave(int& fig_number){
        if (fig_number < 10)
        {
            matplot::save("img/00" + matplot::num2str(fig_number) + ".png", "png");
        }
        else if (fig_number < 100)
        {
            matplot::save("img/0" + matplot::num2str(fig_number) + ".png", "png");
        }
        else
        {
            matplot::save("img/" + matplot::num2str(fig_number) + ".png", "png");
        }
    }
void Functions::plotParticle(double x, double y, double z, double r)
{
    auto funx = [r, x](double u, double v)
    { return r * cos(u) * sin(v) + x; };
    auto funy = [r, y](double u, double v)
    { return r * sin(u) * sin(v) + y; };
    auto funz = [r, z](double u, double v)
    { return r * cos(v) + z; };
    auto g1 = matplot::fsurf(funx, funy, funz, {0, 2 * matplot::pi, 0, matplot::pi}, "", 10);
    // auto g1 = matplot::fmesh(funx, funy, funz, std::array<double, 4>{0, 2 * matplot::pi, 0, matplot::pi});
    g1->font_color("r");
    g1->lighting(matplot::on);
}

void Functions::pushToForceVector(double &F, std::array<double, 3> &unit_R,
                                  std::vector<double> &vec_forceX_A, std::vector<double> &vec_forceY_A, std::vector<double> &vec_forceZ_A)
{
    double unit_forceX_A = F * unit_R.at(0);
    double unit_forceY_A = F * unit_R.at(1);
    double unit_forceZ_A = F * unit_R.at(2);
    vec_forceX_A.push_back(unit_forceX_A);
    vec_forceY_A.push_back(unit_forceY_A);
    vec_forceZ_A.push_back(unit_forceZ_A);
}
bool Functions::testCollision(std::array<double, 3> coordsA, double radiusA, std::array<double, 3> coordsB, double radiusB) // Serve coordinates and radius of particle A and B
{
    double CenterDistance = sqrt((coordsA.at(0) - coordsB.at(0)) * (coordsA.at(0) - coordsB.at(0)) +
                                 (coordsA.at(1) - coordsB.at(1)) * (coordsA.at(1) - coordsB.at(1)) +
                                 (coordsA.at(2) - coordsB.at(2)) * (coordsA.at(2) - coordsB.at(2)));
    double RadiusSum = radiusA + radiusB;
    double SurfaceDistance = CenterDistance - RadiusSum;

    /* std::cout << "--------------------------" << std::endl;
    std::cout << "Test no. " << Number << " Radius: " << Radius << " X: " << Coords.at(0) << std::endl;
    std::cout << "Real no. " << particle_obj.getNumber() << " Radius: " << particle_obj.getRadius() << " X: " << particle_obj.getCoordX() << std::endl;
    std::cout << "Distance: " << CenterDistance << " and Radius sum: " << RadiusSum << " Surf dist: " << SurfaceDistance << std::endl;
*/
    if (SurfaceDistance > 0)
    {
        // std::cout << "\x1B[34mtestCollision() Particles are free.\033[0m\t\t" << std::endl;
        return false;
    }
    else
    {
        // std::cout << "\x1B[31mtestCollision() Particles overlap!\033[0m\t\t" << std::endl;
        return true;
    }
}
/*################
# PARTICLE CLASS #
################*/

std::tuple<double, double, double> Particle::calcForce(std::array<double, 3> coordS_A, double Radius_A,
                                                       std::array<double, 3> coordS_B, double Radius_B)
{
    // Force vector and unit vectors
    double F = 0.0;
    /* double unit_forceX_A;
    double unit_forceY_A;
    double unit_forceZ_A; */
    std::vector<double> vec_forceX_A;
    std::vector<double> vec_forceY_A;
    std::vector<double> vec_forceZ_A;

    // CENTRE DISTANCE
    double R = nm2m * sqrt((coordS_B.at(0) - coordS_A.at(0)) * (coordS_B.at(0) - coordS_A.at(0)) +
                           (coordS_B.at(1) - coordS_A.at(1)) * (coordS_B.at(1) - coordS_A.at(1)) +
                           (coordS_B.at(2) - coordS_A.at(2)) * (coordS_B.at(2) - coordS_A.at(2)));
    // UNIT DISTANCE VECTOR
    std::array<double, 3> unit_R{(coordS_B.at(0) - coordS_A.at(0)) / R * nm2m,
                                 (coordS_B.at(1) - coordS_A.at(1)) / R * nm2m,
                                 (coordS_B.at(2) - coordS_A.at(2)) / R * nm2m};
    // SURFACE DISTANCE
    double D = R - (Radius_A + Radius_B) * nm2m;
    // std::cout << "Centre dist: " << R << " | Surf dist: " << D << std::endl;

    // SIZE OF UNIT VECTOR SHOULD BE = 1
    /* double U = sqrt(unit_R.at(0) * unit_R.at(0) +
                    unit_R.at(1) * unit_R.at(1) +
                    unit_R.at(2) * unit_R.at(2));
    std::cout << U << std::endl; */

    // L-J potential
    double eps = -2.0;
    double sigmaR6 = pow(((Radius_A + Radius_B) * nm2m / R), 6);
    F = 24 * (eps) / R * (2 * sigmaR6 * sigmaR6 - sigmaR6);
    std::cout << "Force: " << F << std::endl;
    Functions::pushToForceVector(F, unit_R, vec_forceX_A, vec_forceY_A, vec_forceZ_A);
    // *L-J potential

    // FINAL SUM FOR PARTICLE 1 <-2 INTERACTION
    double forceX_A = std::accumulate(vec_forceX_A.begin(), vec_forceX_A.end(), 0.0); // https://stackoverflow.com/questions/3221812/how-to-sum-up-elements-of-a-c-vector
    double forceY_A = std::accumulate(vec_forceY_A.begin(), vec_forceY_A.end(), 0.0);
    double forceZ_A = std::accumulate(vec_forceZ_A.begin(), vec_forceZ_A.end(), 0.0);
    return {forceX_A, forceY_A, forceZ_A}; // forceX_A, forceY_A, forceZ_A - for 1 <-2 particle interaction
}

/*############
# REAL CLASS #
############*/
void Real::printRealProperties(std::vector<Real> &real_particles)
{
    std::for_each( // https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
        std::execution::seq,
        real_particles.begin(),
        real_particles.end(),
        [&](auto &particle_obj)
        {
            std::cout << std::endl
                      << "------------------------------------------------" << std::endl
                      << "Particle no. " << particle_obj.getNumber();
            std::cout << std::endl
                      << "XYZ coords [nm]: ";

            for (const double &coord : particle_obj.getCoords())
            { // https://stackoverflow.com/questions/10750057/how-do-i-print-out-the-contents-of-a-vector
                std::cout << coord << " ; ";
            }
            std::cout << std::endl
                      << "Velocities [nm]: ";
            for (const double &velocity : particle_obj.getVelocities())
            { // https://stackoverflow.com/questions/10750057/how-do-i-print-out-the-contents-of-a-vector
                std::cout << velocity << " ; ";
            }
            std::cout << std::endl
                      << "Forces [N]: ";
            for (const double &force : particle_obj.getForces())
            { // https://stackoverflow.com/questions/10750057/how-do-i-print-out-the-contents-of-a-vector
                std::cout << force << " ; ";
            }
            std::cout << std::endl
                      << "Radius [nm]: " << particle_obj.getRadius();
            std::cout << std::endl
                      << "Density [kg/m3]: " << particle_obj.getDensity();
            std::cout << std::endl
                      << "Mass [kg]: " << particle_obj.getMass() << std::endl;
        });
}

void Real::move(double deltaTime, double CoordLimit)
{
    for (int i = 0; i < 3; i++)
    {
        // std::cout << "Part." << Number << ": " << Coords.at(i) << "+" << Velocities.at(i) << "*" << deltaTime << " = ";
        Coords.at(i) = Coords.at(i) + 0.5 * Forces.at(i) / Mass * deltaTime * deltaTime / nm2m + Velocities.at(i) * deltaTime; // Coords at nm, Forces calculated to nm, Velocities at nm
        // std::cout << Coords.at(i) << std::endl;
        if (Coords.at(i) < -CoordLimit || Coords.at(i) >= CoordLimit * 2)
        {
            std::cout << "\x1B[31mERROR: Particle exceeded the box boundaries at coord " << i << ": " << Coords.at(i) << std::endl
                      << "This should not happen! Either the velocity or deltaTime are too large. Exiting ...\033[0m\t\t";
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            std::exit(3); // TODO THIS EXIT !!!
        }
        else if (Coords.at(i) < 0)
        {
            Coords.at(i) = Coords.at(i) + CoordLimit;
        }
        else if (Coords.at(i) >= CoordLimit)
        {
            Coords.at(i) = Coords.at(i) - CoordLimit;
        }
    }
}

void Real::plotParticles(std::vector<Real>& real_particles)
{
    matplot::hold(matplot::on);
    std::for_each( // https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
        std::execution::seq,
        real_particles.begin(),
        real_particles.end(),
        [](Real &particle_obj)
        {
            Functions::plotParticle(particle_obj.getCoordX(), particle_obj.getCoordY(), particle_obj.getCoordZ(), particle_obj.getRadius());
        });
    matplot::colormap({{0, 0, 1}});
    matplot::hold(matplot::off);
}

/*############
# VIRT CLASS #
############*/

void Virt::plotParticles(std::vector<Virt> virt_particles)
{
    matplot::hold(matplot::on);
    std::for_each( // https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
        std::execution::seq,
        virt_particles.begin(),
        virt_particles.end(),
        [](Virt &particle_obj)
        {
            Functions::plotParticle(particle_obj.getCoordX(), particle_obj.getCoordY(), particle_obj.getCoordZ(), particle_obj.getRadius());
        });
    matplot::colormap({{0, 1, 0}});
    matplot::hold(matplot::off);
}