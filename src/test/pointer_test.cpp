#include <iostream>

void copyValue(int a)
{
    std::cout << "Copy      before: " << a << std::endl;
    a = 1;
}
void pointer(int *a)
{
    std::cout << "Pointer   before: " << *a << std::endl;
    *a = 1;
}
void reference(int &a)
{
    std::cout << "Reference before: " << a << std::endl;
    a = 1;
}
/////////////////////////////////////
int modFrequency(int &Frequency, int modulus)
{
    return Frequency % modulus;
}

class Device
{
private:
    int Frequency;

public:
    Device(int frequency)
    {
        Frequency = frequency;
    }
    int getFrequency() { return Frequency; }
    int modFrequency(int modulus)
    {
        return Frequency % modulus;
    }
};

int main()
{
    int a = 0;
    copyValue(a);
    std::cout << "Copy       after: " << a << std::endl;

    a = 0;
    pointer(&a);
    std::cout << "Pointer    after: " << a << std::endl;

    a = 0;
    reference(a);
    std::cout << "Reference  after: " << a << std::endl;

    /////////////////////////////////////
    int Frequency1 = 1500;
    std::cout << Frequency1 << std::endl;
    std::cout << Frequency1 % 1000 << std::endl;

    std::cout << Frequency1 << std::endl;
    std::cout << modFrequency(Frequency1, 1000) << std::endl;

    Device multimeter1 = Device(1500);
    std::cout << multimeter1.getFrequency() << std::endl;
    std::cout << multimeter1.modFrequency(1000) << std::endl;

    
    return 0;
}