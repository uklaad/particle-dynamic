#include <matplot/matplot.h>
#include "../include/video.h"
#include <tuple>
#include <string>
#include <vector>
#include <iostream>
#include <cmath>

void plotParticle (double x, double y, double z, double r) {   
    auto funx = [r,x](double u, double v) { return r * cos(u) * sin(v)+x; };
    auto funy = [r,y](double u, double v) { return r * sin(u) * sin(v)+y; };
    auto funz = [r,z](double u, double v) { return r * cos(v)+z; };
    auto g1 = matplot::fsurf(funx, funy, funz, {0, 2*matplot::pi, 0, matplot::pi}, "", 20);
    //auto g1 = matplot::fmesh(funx, funy, funz, std::array<double, 4>{0, 2 * matplot::pi, 0, matplot::pi});
}
std::tuple<std::vector<double>, std::vector<double>, std::vector<double>, std::vector<double>, std::vector<double>, std::vector<double>, std::vector<double>, std::vector<double>>
collectPlotData() {
    using namespace matplot;
    std::vector<double> x_basic = { 0, 200 };
    std::vector<double> y_basic = { 0, 0 };
    std::vector<double> z_basic = { 0, 0 };
    std::vector<double> size_basic = { 100, 100 };

    std::vector<double> x_hl = { 200 };
    std::vector<double> y_hl = { 0 };
    std::vector<double> z_hl = { 0 };
    std::vector<double> size_hl = { 100 };

    return std::make_tuple(x_basic, y_basic, z_basic, size_basic, x_hl, y_hl, z_hl, size_hl);
}
int main() {
    double AxLimit = 1000;
    
    auto fig = matplot::figure(true);
    fig->size(1000,1000);
    auto graph = matplot::gca();
    matplot::xlim({0, AxLimit});  matplot::ylim({0, AxLimit});  matplot::zlim({0, AxLimit});
    matplot::box(matplot::on);    graph->box_full(true);

    matplot::hold(matplot::on);
    matplot::colormap({{0, 0, 1}});
    plotParticle(100,100,100,100);
    matplot::colormap({{0, 1, 0}});
    plotParticle(500,500,500,200);
    matplot::hold(matplot::off);

    matplot::show(); // <- HOW TO GET THIS IMAGE INTO THE cv::Mat img format below to be able to make the video?
//********************************************************************************
    int width = 640;
    int height = 480;
    int n_frames = 100;
    int FPS = 24;

    // Generate 100 synthetic JPEG encoded images in memory:
    //////////////////////////////////////////////////////////////////////////
    std::list<std::vector<uchar>> jpeg_frames;

    for (int i = 0; i < n_frames; i++)
    {
        cv::Mat img = cv::Mat(height, width, CV_8UC3);
        img = cv::Scalar(60, 60, 60);
        cv::putText(img, std::to_string(i + 1), cv::Point(width / 2 - 100 * (int)(std::to_string(i + 1).length()), height / 2 + 100), cv::FONT_HERSHEY_DUPLEX, 10, cv::Scalar(30, 255, 30), 20); // Green number
        //cv::imshow("img", img);cv::waitKey(1);
        std::vector<uchar> jpeg_img;
        cv::imencode(".JPEG", img, jpeg_img);
        jpeg_frames.push_back(jpeg_img);
    }
    //////////////////////////////////////////////////////////////////////////
    makeVideo(jpeg_frames, FPS);


{    
/*     auto [x_basic, y_basic, z_basic, size_basic, x_hl, y_hl, z_hl, size_hl] = collectPlotData();
    double AxLimit = 400;

    auto fig = matplot::figure(true);
    fig->size(500,500);
    auto graph = matplot::gca();
    
    graph->colororder(std::vector<std::string> {"cyan", "magenta", "blue", "blue"});  
    graph->xlim({0, AxLimit});  graph->ylim({0, AxLimit});  graph->zlim({0, AxLimit});
    matplot::box(matplot::on);    graph->box_full(true);

    graph->hold(matplot::on);
    graph->scatter3(x_basic, y_basic, z_basic, size_basic)->marker_face(true);
    graph->scatter3(std::vector<double> {}, std::vector<double> {}, std::vector<double> {}, std::vector<double> {})->marker_face(true);
    graph->scatter3(x_hl, y_hl, z_hl, size_hl)->marker_face(true);
    
    matplot::vector_1d x = {1};
    matplot::vector_1d y = {0};
    matplot::vector_1d z = {0};
    matplot::vector_1d u = {1};
    matplot::vector_1d v = {1};
    matplot::vector_1d w = {1};
    matplot::vector_1d m = {0.1};
    
    
    
    graph->quiver3(x, y, z, u, v, w, m)->normalize(false).line_width(2);
    graph->colormap(matplot::palette::jet());
    graph->hold(matplot::off);
    matplot::view(0,0);
    matplot::save("img/barchart.svg");
    matplot::save("img/barchart.gif");
    matplot::save("img/barchart.jpg");
    matplot::save("img/barchart.png");
    //getchar(); */
}
    return 0;
}


