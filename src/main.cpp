#include "include/particle.h"
#include "include/readConfig.h"

/*
Defined classes are in /include/ *.h files, which contain heading #include <iostream> etc.
Classes' functions are in supporting *.cpp files of the same name.
*/
#include <chrono>
#include <thread>

//################################################
/**
 * @brief Global random generator
 * @details Seed for random generation of particle's coordinates (normal distribution), velocities (normal distribution) and radius (log-normal distribution).\n
 * You can choose from deterministic seed, or non-deterministic random seed by (un)commenting in the code.
 */
std::default_random_engine generator; // Deterministic http://www.cplusplus.com/forum/beginner/223069/
// std::random_device rd; std::default_random_engine generator(rd()); // Random https://stackoverflow.com/questions/15461140/stddefault-random-engine-generate-values-between-0-0-and-1-0
//################################################
/**
 * @brief Global constant for calculation speedup
 * @details Conversion from nm to m (10^-9)
 */
double nm2m = 0.000000001;
/*###########
# FUNCTIONS #
###########*/
/**
 * @brief Duplicate Real particles to surroundings
 *
 * @param real_particles | Vector containing Real particle objects.
 * @param virt_particles | Vector for Virt particle objects.
 */
void genVirtParticles(std::vector<Real> &real_particles, std::vector<Virt> &virt_particles)
{
    virt_particles.clear();
    virt_particles.reserve(real_particles.size()); // Speed optimalization
    std::for_each(                                 // https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
        std::execution::seq,
        real_particles.begin(),
        real_particles.end(),
        [&](Real &particle_obj)
        {
            { // 1: -x-y
                Virt test_particle = Virt(particle_obj.getCoordX() - CoordLimit, particle_obj.getCoordY() - CoordLimit, particle_obj.getCoordZ(), particle_obj.getRadius());
                virt_particles.push_back(test_particle);
            }
            { // 2: -y
                Virt test_particle = Virt(particle_obj.getCoordX(), particle_obj.getCoordY() - CoordLimit, particle_obj.getCoordZ(), particle_obj.getRadius());
                virt_particles.push_back(test_particle);
            }
            { // 3: +x-y
                Virt test_particle = Virt(particle_obj.getCoordX() + CoordLimit, particle_obj.getCoordY() - CoordLimit, particle_obj.getCoordZ(), particle_obj.getRadius());
                virt_particles.push_back(test_particle);
            }
            { // 6: +x
                Virt test_particle = Virt(particle_obj.getCoordX() + CoordLimit, particle_obj.getCoordY(), particle_obj.getCoordZ(), particle_obj.getRadius());
                virt_particles.push_back(test_particle);
            }
            { // 9: +x+y
                Virt test_particle = Virt(particle_obj.getCoordX() + CoordLimit, particle_obj.getCoordY() + CoordLimit, particle_obj.getCoordZ(), particle_obj.getRadius());
                virt_particles.push_back(test_particle);
            }
            { // 8: +y
                Virt test_particle = Virt(particle_obj.getCoordX(), particle_obj.getCoordY() + CoordLimit, particle_obj.getCoordZ(), particle_obj.getRadius());
                virt_particles.push_back(test_particle);
            }
            { // 7: -x+y
                Virt test_particle = Virt(particle_obj.getCoordX() - CoordLimit, particle_obj.getCoordY() + CoordLimit, particle_obj.getCoordZ(), particle_obj.getRadius());
                virt_particles.push_back(test_particle);
            }
            { // 4: -x
                Virt test_particle = Virt(particle_obj.getCoordX() - CoordLimit, particle_obj.getCoordY(), particle_obj.getCoordZ(), particle_obj.getRadius());
                virt_particles.push_back(test_particle);
            }
        });
}

/**
 * @brief Generates Real particle of set properties that does not collide with existing Real.
 * @details
 *
 * @param real_particles | Vector containing Real particle objects.
 * @param number | Particle's no.
 * @param CoordLimit | Size of the unit box defined in the parameters.conf
 * @param VelocityLimit | Initial max particle's velocity defined in the parameters.conf
 * @param Mu | Log-normal parameter defined in the parameters.conf for radius creation.
 * @param Sigma | Log-normal parameter defined in the parameters.conf for radius creation.
 * @param Density | Particle's density (material property).
 * @return true | New particle creation reached maximum attempts and still overlaps existing Real. EXIT()
 * @return false | Particle creation went fine and the program can proceed.
 */
bool calcNewReal(std::vector<Real> &real_particles, std::vector<Virt> &virt_particles, int &number, double &CoordLimit, double &VelocityLimit, double &Mu, double &Sigma, double &Density)
{ // Create new particle and check if it does not overlap with others!
    int MaxTrials = 0;
    bool Collision;
    genVirtParticles(real_particles, virt_particles);
    do
    {
        // Generate test particle
        Real test_particle = Real(number, CoordLimit, VelocityLimit, Mu, Sigma, Density);

        MaxTrials++;
        Collision = false;

        // Test the newly generated particle if it does not collide with already existing
        // The for loop does not activate when there is only one real particle
        for (long unsigned int i = 0; i < real_particles.size(); i++)
        {
            if (Functions::testCollision(test_particle.getCoords(), test_particle.getRadius(), real_particles.at(i).getCoords(), real_particles.at(i).getRadius()))
            {
                // std::cout << "\x1B[31mcalcNewReal() Bool returned true, not making particle\033[0m\t\t" << std::endl
                //           << std::endl;
                Collision = true;
                break;
            }
            else
            {
                // std::cout << "\x1B[34mcalcNewReal() Bool returned false\033[0m\t\t" << std::endl
                //           << std::endl;
            }
        }
        if (!Collision)
        {
            for (long unsigned int i = 0; i < virt_particles.size(); i++)
            {
                if (Functions::testCollision(test_particle.getCoords(), test_particle.getRadius(), virt_particles.at(i).getCoords(), virt_particles.at(i).getRadius()))
                {
                    // std::cout << "\x1B[31mcalcNewReal() Bool returned true, not making particle\033[0m\t\t" << std::endl
                    //           << std::endl;
                    Collision = true;
                    break;
                }
                else
                {
                    // std::cout << "\x1B[34mcalcNewReal() Bool returned false\033[0m\t\t" << std::endl
                    //           << std::endl;
                }
            }
        }

        // If the collision test is false (no collision), push the created particle to Real vector
        if (!Collision)
        {
            real_particles.push_back(test_particle);
            // std::cout << "Created with: " << MaxTrials << " trials" << std::endl;
        }

    } while (Collision && MaxTrials <= 100000);

    if (MaxTrials > 100000)
    {
        return true; // Too much collisions, no success
    }
    else
    {
        return false; // Particle was successfuly created
    }
}

/**
 * @brief
 *
 * @param real_particles
 * @param virt_particles
 */
void calcForces(std::vector<Real> &real_particles, std::vector<Virt> &virt_particles)
{
    /* double sigma = 1.0;
    double sigma_m = pow(2, (1.0 / 6.0)) * sigma;
    std::cout << "sigma: " << sigma_m << std::endl;
    auto [forceX_A, forceY_A, forceZ_A] = Particle::calcForce({0, 0, 0}, 0.5, {sigma_m, 0, 0}, 0.5); // XYZR ;
    std::cout << forceX_A << " | " << forceY_A << " | " << forceZ_A << std::endl; */

    std::for_each( // https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
        std::execution::par,
        real_particles.begin(),
        real_particles.end(),
        [&](Real &real_A)
        {
            // Real x Real
            std::vector<double> RxRForces(3);
            std::vector<double> RxRForceX;
            std::vector<double> RxRForceY;
            std::vector<double> RxRForceZ;
            std::for_each(
                std::execution::seq,
                real_particles.begin(),
                real_particles.end(),
                [&](Real &real_B)
                {
                    if (real_A.getNumber() != real_B.getNumber())
                    {
                        // std::cout << real_A.getNumber() << " x " << real_B.getNumber() << std::endl;
                        auto [forceX_A, forceY_A, forceZ_A] = Particle::calcForce(real_A.getCoords(), real_A.getRadius(),
                                                                                  real_B.getCoords(), real_B.getRadius());
                        // std::cout << forceX_A << " | " << forceY_A << " | " << forceZ_A << std::endl;

                        RxRForceX.push_back(forceX_A);
                        RxRForceY.push_back(forceY_A);
                        RxRForceZ.push_back(forceZ_A);
                    }
                });
            RxRForces = {std::accumulate(RxRForceX.begin(), RxRForceX.end(), 0.0),
                         std::accumulate(RxRForceY.begin(), RxRForceY.end(), 0.0),
                         std::accumulate(RxRForceZ.begin(), RxRForceZ.end(), 0.0)};

            // Real x Virt
            std::vector<double> RxVForces(3);
            std::vector<double> RxVForceX;
            std::vector<double> RxVForceY;
            std::vector<double> RxVForceZ;
            std::for_each(
                std::execution::seq,
                virt_particles.begin(),
                virt_particles.end(),
                [&](Virt &virt_A)
                {
                    auto [forceX_A, forceY_A, forceZ_A] = Particle::calcForce(real_A.getCoords(), real_A.getRadius(),
                                                                              virt_A.getCoords(), virt_A.getRadius());
                    // std::cout << forceX_A << " | " << forceY_A << " | " << forceZ_A << std::endl;
                    RxVForceX.push_back(forceX_A);
                    RxVForceY.push_back(forceY_A);
                    RxVForceZ.push_back(forceZ_A);
                });
            RxVForces = {std::accumulate(RxVForceX.begin(), RxVForceX.end(), 0.0),
                         std::accumulate(RxVForceY.begin(), RxVForceY.end(), 0.0),
                         std::accumulate(RxVForceZ.begin(), RxVForceZ.end(), 0.0)};

            std::cout << "Particle no. " << real_A.getNumber() << std::endl
                      << "-------------------------" << std::endl;
            std::cout << "RxRForces: ";
            for (long unsigned int i = 0; i < RxRForces.size(); i++)
            {
                std::cout << RxRForces.at(i) << " | ";
            }
            std::cout << std::endl
                      << "RxVForces: ";
            ;
            for (long unsigned int i = 0; i < RxRForces.size(); i++)
            {
                std::cout << RxVForces.at(i) << " | ";
            }
        });
}

/**
 * @brief Calculate new particles' coordinates according to the acting force and actual velocity.
 * @details If the particle's position exceeds unit box's boarders (defined by CoordLimit), the particle is moved in from the other side.
 * If the particle leaves double the box (double the CoordLimit), something is wrong and program EXIT()
 * @param real_particles | Vector containing Real particle objects.
 * @param deltaTime | Time step defined in the parameters.conf
 * @param CoordLimit | Size of the unit box defined in the parameters.conf
 */
void calcNewCoords(std::vector<Real> &real_particles, double &deltaTime, double &CoordLimit)
{
    std::for_each( // https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
        std::execution::seq,
        real_particles.begin(),
        real_particles.end(),
        [&](Real &particle_obj)
        {
            particle_obj.move(deltaTime, CoordLimit);
        });
}
/*############
# /FUNCTIONS #
############*/

int main()
{

    // PARAMETERS INIT
    LoadParameters(); // From parameters.conf
    // *PARAMETERS INIT

    // PARTICLES LISTS INIT
    std::vector<Real> real_particles; // particles.reserve(20); https://stackoverflow.com/questions/5226352/declare-an-array-of-objects-using-a-for-loop-c
    std::vector<Virt> virt_particles;
    // *PARTICLES LISTS INIT

    // FIGURE INIT
    Functions::setPlot(CoordLimit); // Plot init
    // *FIGURE INIT

    // REAL PARTICLES GENERATION
    for (int number = 0; number < MaxPartNumber; ++number)
    {
        if (calcNewReal(real_particles, virt_particles, number, CoordLimit, VelocityLimit, Mu, Sigma, Density)) // Create particles and check if it met max iteration limit -> break
        {
            std::cout << "\x1B[31mParticle overload, reached 100 000 attempts - cannot generate more! Exitting ...\033[0m\t\t" << std::endl;
            break;
        }
    }
    // *REAL PARTICLES GENERATION

    // VIRT PARTICLES GENERATION
    genVirtParticles(real_particles, virt_particles);
    std::cout << "Real: " << real_particles.size() << " Virt: " << virt_particles.size() << " | " << real_particles.size() * 8 << std::endl;

    Real::plotParticles(real_particles);
    Virt::plotParticles(virt_particles);
    matplot::show();
    // std::this_thread::sleep_for(std::chrono::milliseconds(20));
    //  *VIRT PARTICLES GENERATION

    // CALC FORCES - real/real ; real/virt
    calcForces(real_particles, virt_particles);
    // *CALC FORCES

    // PARTICLE MOVEMENT (just test movement)
    for (int i = 0; i < 10; ++i)
    {
        calcNewCoords(real_particles, deltaTime, CoordLimit);
        genVirtParticles(real_particles, virt_particles);
        matplot::gca()->children({}); // Plot cleanup
        Real::plotParticles(real_particles);
        Virt::plotParticles(virt_particles);
        matplot::title("Test Real+Virt (XY max 10 nm/s): " + matplot::num2str(deltaTime * i) + " s");
        Functions::figSave(i);
    }
    // *PARTICLE MOVEMENT

    // CREATING VIDEO
    // system("ffmpeg -loglevel warning -framerate 24 -i \"img/%03d.png\" output.mkv");
    // *CREATING VIDEO
    
    // Real::printRealProperties(real_particles);

    /*############## SOME GARBAGE ####################################################################################################

        // TESTING COLLISION IN REAL VECTOR
        std::for_each( // https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
            std::execution::seq,
            real_particles.begin(),
            real_particles.end(),
            [&](auto &particle_obj)
            {
                particle_obj.testCollision(real_particles);
            });

        Real::plotParticles(real_particles);
        matplot::show();
        */
    /*


        // PLOTTING
        Real::plotParticles(real_particles);
        matplot::show();
        // Functions::setPlot(CoordLimit);
        Virt::plotParticles(virt_particles);
        matplot::show();

        // JUST SOME OUTPUT
        std::for_each( // https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
            std::execution::seq,
            real_particles.begin(),
            real_particles.end(),
            [&](auto &particle_obj)
            {
                particle_obj.testCollision(real_particles);
                std::cout << "Particle no. " << particle_obj.getNumber() << " xyz coords:" << std::endl
                          << "for_each loop: ";

                std::for_each_n(
                    std::execution::seq,
                    particle_obj.getCoords().begin(),
                    3, // particle_obj.getCoords().end(),  --this does not work
                    [](const double &coord)
                    {
                        std::cout << coord << " ";
                    });
                std::cout << std::endl
                          << "ranged loop:   ";
                for (const double &coord : particle_obj.getCoords())
                { // https://stackoverflow.com/questions/10750057/how-do-i-print-out-the-contents-of-a-vector
                    std::cout << coord << " ";
                }
                std::cout << std::endl
                          << "just manual:   " << particle_obj.getCoordX() << " " << particle_obj.getCoordY() << " " << particle_obj.getCoordZ() << std::endl
                          << std::endl;
            });
    */
    std::cout << std::endl
              << "Hi, the program ran to the end.";
    return 0;
}